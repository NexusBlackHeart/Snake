// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_pawn_base.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"

// Sets default values
APlayer_pawn_base::APlayer_pawn_base()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayer_pawn_base::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));

	CreateSnakeActor();
	
}

// Called every frame
void APlayer_pawn_base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayer_pawn_base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayer_pawn_base::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

